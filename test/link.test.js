const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')
const CrutByType = require('ssb-whakapapa/lib/crut-by-type')

const Server = require('./test-bot')
const { TestGroup } = require('./lib/helpers')

test('link.create invalid input', async t => {
  t.plan(4)
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)
  const validParentId = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, { preferredName: 'Cherese' }, {})
  const validChildId = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, { preferredName: 'Claudine' }, {})

  const invalidSubmissionId = '%T6nCNtRa/Q1f5MtbTd2jF/zrB41XOrIg5hD/IK+t8Jk=.sha256'

  const profileInput = {
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    }
  }
  const randomProfileId = await p(ssb.profile.person.public.create)(profileInput)

  const defaultValidFields = {
    parentId: validParentId,
    childId: validChildId
  }

  const failCases = [
    {
      parentId: invalidSubmissionId,
      expectedError: /not found in leveldb index/
    },
    {
      childId: invalidSubmissionId,
      expectedError: /not found in leveldb index/
    },
    {
      parentId: randomProfileId,
      expectedError: /to be of type submissions, got profile\/person instead./
    },
    {
      childId: randomProfileId,
      expectedError: /to be of type submissions, got profile\/person instead./
    }
  ]

  await Promise.all(
    failCases.map(async input => {
      const {
        parentId,
        childId,
        expectedError
      } = ({ ...defaultValidFields, ...input })

      delete input.expectedError

      return p(ssb.submissions.link.create)(parentId, childId, {})
        .then(() => t.fail(`expected ${expectedError} to fail, but it passed instead`))
        .catch(err => t.match(err.message, expectedError, `submission.proposeEdit invalid input ${JSON.stringify({ parentId, childId })} throws error`))
    })
  )
})

test('link crut (simple)', async t => {
  t.plan(4)
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)
  const submissionA = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, { preferredName: 'Cherese' }, {})
  const submissionB = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, { preferredName: 'Claudine' }, {})

  const linkId = await p(ssb.submissions.link.create)(submissionA, submissionB, {})

  t.true(isMsgId(linkId), 'link.create returns a valid messageId')

  const link = await p(ssb.submissions.link.get)(linkId)

  const expectedLink = {
    key: linkId,
    type: 'link/submissions',
    originalAuthor: ssb.id,
    recps: null,
    parent: submissionA,
    child: submissionB,
    conflictFields: [],
    mappedDependencyFields: null,
    states: [
      {
        key: linkId,
        tombstone: null
      }
    ],
    tombstone: null
  }

  t.deepEqual(
    link,
    expectedLink,
    'returns the link'
  )

  const links = await p(ssb.submissions.link.list)()

  t.deepEqual(
    links,
    [expectedLink],
    'returns the link in the list'
  )

  // get links that have a certain parent

  const childLinks = await p(ssb.submissions.link.list)(
    {
      filter: link => link.parent === submissionA
    }
  )

  t.deepEqual(
    childLinks,
    links,
    'list with parent works'
  )
})

test('link (recps)', async t => {
  t.plan(3)

  const {
    kaitiaki,
    member,
    groupId,
    poBoxId
  } = await TestGroup()

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  kaitiaki.submissions.registerHandler(kaitiaki.profile.person.group)
  kaitiaki.submissions.registerHandler(CrutByType(kaitiaki)('link/profile-profile/child'))

  member.submissions.registerHandler(member.profile.person.group)
  member.submissions.registerHandler(CrutByType(member)('link/profile-profile/child'))

  // 1. Kaitiaki creates a profile as the "parent" in the relationship
  const parentInput = {
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const mumsProfileId = await p(kaitiaki.profile.person.group.create)(parentInput)

  // 2. Member proposes to add a new "child" to the "parent"
  const submissionIdA = await p(member.submissions.proposeNew)(
    { type: 'profile/person' },
    {
      preferredName: 'Cherese',
      authors: {
        add: ['*']
      },
      recps: [groupId]
    },
    {
      groupId,
      comment: 'I want to add my child',
      recps: [poBoxId, member.id]
    }
  ).catch(err => console.error('failed proposing new submission', err))

  const linkDetails = {
    parent: mumsProfileId,
    // NOTE: we leave this empty on purpose, and the value will be filled in later
    // this is classed as a dependent field.. TODO: how do we flag a field as dependent
    // child: undefined
    relationshipType: 'birth',
    legallyAdopted: false
  }

  const submissionIdB = await p(member.submissions.proposeNew)(
    { type: 'link/profile-profile/child' },
    linkDetails,
    {
      groupId,
      recps: [poBoxId, member.id]
    }
  )

  await p(member.submissions.link.create)(
    submissionIdA, // parent
    submissionIdB, // child
    {} // No recps here will add the recps from the parent submission instead
  )

  await member._replicate(kaitiaki)
  await kaitiaki._replicate(member)

  // attempt to approve the dependent submission
  await p(kaitiaki.submissions.executeAndApprove)(submissionIdB, linkDetails, {})
    .then(() => t.fail('Should have thrown an error'))
    .catch(err => {
      t.deepEqual(
        err.message,
        `submissions.execute Submission with id: ${submissionIdB} is dependent on the submission with id: ${submissionIdA} being approved`,
        'threw an error when trying to approve a dependent submission where the parent hasnt been approved'
      )
    })

  await member._replicate(kaitiaki)
  await kaitiaki._replicate(member)

  const links = await p(member.submissions.link.list)({
    filter: link => link.parent === submissionIdA
  })

  t.equals(links.length, 1, 'returns one link')

  t.deepEquals(
    links[0].recps,
    [poBoxId, member.id],
    'link was encrypted to the same recps as the submission'
  )
})
