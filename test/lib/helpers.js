const { promisify: p } = require('util')
const Server = require('../test-bot')

function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

async function TestGroup (opts = {}) {
  const kaitiaki = Server({ tribes: true, ...opts })
  const member = Server({ tribes: true, ...opts })

  // kaitiaki sets up the group
  const { groupId, poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  // kaitiaki adds the member to the group
  await p(kaitiaki.tribes.invite)(groupId, [member.id], {})

  await kaitiaki._replicate(member)
  await member._replicate(kaitiaki)

  // wait for rebuild
  await sleep(500)

  return {
    kaitiaki,
    member,
    groupId,
    poBoxId
  }
}

module.exports = {
  TestGroup,
  sleep
}
