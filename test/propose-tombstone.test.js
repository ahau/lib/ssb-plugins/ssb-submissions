const test = require('tape')
const { promisify: p } = require('util')
const { isMsgId } = require('ssb-ref')

const Server = require('./test-bot')

test('submissions.proposeTombstone (profile/person public)', async t => {
  t.plan(5)
  const ssb = Server()
  t.teardown(ssb.close)

  const profileDetails = {
    preferredName: 'Col',
    authors: { add: [ssb.id] }
  }

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const profileId = await p(ssb.profile.person.public.create)(profileDetails)
  t.true(isMsgId(profileId), 'created a profile')

  const submissionId = await p(ssb.submissions.proposeTombstone)(profileId, { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' })
  t.true(isMsgId(submissionId), 'creates a new submission')

  let submission = await p(ssb.submissions.get)(submissionId)

  const tombstone = submission.details.tombstone
  t.true(tombstone && tombstone.date, 'submission saved with a tombstone and date')

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      source: null,
      details: {
        tombstone
      },
      moreInfo: null,
      conflictFields: [],
      states: [{
        key: submissionId,
        targetId: null,
        tombstone: null,
        approvedBy: [],
        rejectedBy: [],
        comments: {}
      }],
      tombstone: null,
      approvedBy: [],
      rejectedBy: [],
      comments: {}
    },
    'gets the submission and the tombstone was propsed on the submission'
  )

  await p(ssb.submissions.approve)(submissionId, {})

  submission = await p(ssb.submissions.get)(submissionId)
  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      source: null,
      details: {
        tombstone
      },
      moreInfo: null,
      conflictFields: [],
      states: [{
        key: submission.states[0].key,
        targetId: null,
        tombstone: null,
        approvedBy: [ssb.id],
        rejectedBy: [],
        comments: {}
      }],
      tombstone: null,
      approvedBy: [ssb.id],
      rejectedBy: [],
      comments: {}
    },
    'gets the updated approved submission'
  )

  // NOTE: testing whether a profile was actually tombstoned will be done in the approve.test.js
})
