const test = require('tape')
const Server = require('./test-bot')

const details = () => ({
  preferredName: 'Colin'

})

test('simple comment', t => {
  const server = Server()
  const kaitiaki = server.id
  const profileDetails = {
    preferredName: 'Col',
    authors: { add: [kaitiaki] }
  }
  server.submissions.registerHandler(server.profile.person.public)

  server.profile.person.public.create(profileDetails, (err, profileId) => {
    t.error(err, 'Profile created')

    server.submissions.proposeEdit(profileId, details(), { comment: 'Fix name', groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' }, (err, submissionId) => {
      t.error(err, 'Submission created with no error')
      server.submissions.get(submissionId, (err, data) => {
        t.error(err, 'No error when reading data')
        t.deepEqual(data.comments, { [kaitiaki]: 'Fix name' }, 'Comment has been added')
        server.submissions.approve(submissionId, { }, (err, approveedId) => {
          t.error(err, 'Submission approveed with no error')
          server.submissions.get(submissionId, (err, data) => {
            t.error(err, 'No error when reading data')
            t.deepEqual(data.comments, { [kaitiaki]: 'Fix name' }, 'Comment has not been changed')
            server.submissions.reject(submissionId, { comment: 'Incorrect name' }, (err, approvedId) => {
              t.error(err, 'Submission rejected with no error')
              server.submissions.get(submissionId, (err, data) => {
                t.error(err, 'No error when reading data')
                t.deepEqual(data.comments, { [kaitiaki]: 'Incorrect name' }, 'Comment has been overwritten')
                server.close()
                t.end()
              })
            })
          })
        })
      })
    })
  })
})
