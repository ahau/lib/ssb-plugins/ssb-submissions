const API = require('./method')

const crut = {
  // Create:
  proposeEdit: 'async',
  proposeNew: 'async',
  proposeTombstone: 'async',
  approve: 'async',
  reject: 'async',

  get: 'async',
  // update: 'async', // just do a new submission for now
  tombstone: 'async',
  list: 'async',
  link: {
    create: 'async',
    get: 'async',
    list: 'async'
  }
}

module.exports = {
  name: 'submissions',
  version: require('./package.json').version,
  manifest: {
    ...crut,
    showProposalResult: 'async',
    registerHandler: 'async',
    findHandler: 'sync'
  },
  init: (ssb) => {
    return API(ssb)
  }
}
