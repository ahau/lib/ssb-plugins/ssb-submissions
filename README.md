# ssb-submissions

An `ssb-server` plugin for creating, reading and updating submissions in `scuttlebutt`

## Modules

* ssb-submissions

---

## Submissions

A submission is a request for another record to be created, updated, or tombstoned, that is stored using ssb.

```js
   A   (the root message)
   |
   B   (approveing or rejecting the proposal A)
  / \
 C   D (two concurrent approves/rejects after B)
```

Because there might be multiple offline approves/rejects to a submission which didn't know about one-another, it's possible for divergence to happen:
`C` and `D` wont know about each other.

A submission is an Object which maps the key of each latest edit to the state it perceives the submission to be in:

```js
// submission for the example above
{
  key: MessageId, // submissionId supplied, the root message of the submission
  states: [
    { head: C, state: stateC },
    { head: D, state: stateD },
  ]
}
```

and the `state` is an Object which looks like the following:

```js
{
  // static fields
  sourceId: msgId,
  targetType: String,
  details: Object,
  source: RecordSource,

  //
  targetId: msgId,
  approvedBy: SimpleSet of feedIds,
  rejectedBy: SimpleSet of feedIds,
  comments: OverWriteFields of feedIds: String
}
```

where:
- `sourceId` - is the original record e.g. `profileId`, the submission proposal is about. E.g. if we are proposing to edit a profile, the `sourceId` would be the profile we are updating. If we are proposing a new profile, the `sourceId` is always empty.
- `targetId` - is the "result" record after a submission has been executed. E.g. if we have proposed and executed a submission for a new profile, the `targetId` is the `profileId` of the new profile that was created. If we have proposed and executed a submission for an edit to a profile, the `targetId` is the `msgId` (`profileId`) of the update message to that profile.
- `targetType` - is the `type` of record we are proposing, and the `type` of record we will get in the result of `targetId` after we `execute` the proposal.
- `details` - is the proposed fields we wish to include in the type of record we are creating or updating. E.g. If we are proposing a new `profile`, the `details` will be `fields` accepted on the record type `profile/person`.
- `source` *RecordSource* (ahau|webForm) tells us where a record, in this case a `submission`, came from. E.g. `ahau` - a record was made in ahau, `webForm` a record was made from a web registration form
---

## API

### `server.submissions.registerHandler(crut)`

In order to check that the proposals are valid, begin by registering a crut that contains:
* `spec.type` *String* - the type of the target record
TODO:
* `isValid` *Function* - allows you to check the validity of the proposal.

Important: currently uses an extended CRUT, by adding ...crut to the Methods of the desired record.
Example use case:
```js
  server.submissions.registerHandler(server.profile.person.public)
```

### `server.submissions.findHandler(content) => crut`


### `server.submissions.proposeNew(recordTypeContent, recordDetails, submissionDetails, cb)`

Creates a new submission record and describes details you'd like to have on a new type of record

* `recordTypeContent` *Object* - content for the type of record you would like to create
  * e.g. for a `profile/person` record:
  
  ```javascript
  recordTypeContent = {
    type: 'profile/person'
  }
  ```

* `recordDetails` *Object* - allows you to propose fields for new record.
  * e.g. for a `profile/person` record:

    ```javascript
    // for profile/person
    {
      preferredName: 'Colin'
    }
    ```

  NOTE: the fields will be validated against the spec for that type of record using the `recordTypeContent`

* `submissionDetails` *Object* - additional details you can add to the submission
  * `comment` *String* - allows you to add an optional comment to the submission.
  * `source` *RecordSource* (ahau|webForm) - specify where the submission is being created, e.g. from `ahau` or from a web registration form `webForm`
  * `moreInfo` *Object* - specify additional information to be saved to the submission.

      ```javascript
      {
        [key]: [
          {
            label: String,
            value: String
          }
        ]
      }
      ```

      > NOTE: This is currently the only format that is supported. If more formats are needed, they can be added

  * `recps` *[String]* - Who is allowed to see this submission. The new record may have different recps.
      // TODO: Submissions should only be seen by kaitiaki group
* `cb` *Function* - a callback with signature (err, submissionId)

---

### `server.submissions.proposeEdit(recordId, recordDetails, submissionDetails, cb)`

Creates a new submission record and describes details you'd like to have updated on an existing record

* `recordId` *String* - the cypherlink for the target record (the `msgId` of the root message for the record)
* `recordDetails` *Object* - allows you to propose fields for new record.
  * e.g. for a `profile/person` record:

    ```javascript
    // for profile/person
    {
      preferredName: 'Colin'
    }
    ```

  NOTE: the fields will be validated against the spec for that type of record that recordId points to
* `submissionDetails` *Object* - additional details you can add to the submission
  * `comment` *String* - allows you to add an optional comment to the submission.
  * `recps` *[String]* - Who is allowed to see this submission. The target record may have different recps.
      // TODO: Submissions should only be seen by kaitiaki group
* `cb` *Function* - a callback with signature (err, submissionId)

---

### `server.submissions.proposeTombstone(recordId, submissionDetails, cb)`

Creates a new submission record about which existing record you want tombstoned

* `recordId` *String* - the cypherlink for the target record (the `msgId` of the root message for the record)
* `submissionDetails` *Object* - additional details you can add to the submission
  * `comment` *{ String }* - allows you to add an optional comment to the submission.
* `cb` *Function* - a callback with signature `(err, submissionId)`

---

### `server.submissions.approve(submissionId, submissionDetails, cb)`

Updates a submission record by setting server.id to approvedBy and adding the comment

* `submissionId` *String* - the cypherlink for the target submission (the `msgId` of the root message for the submission)
* `submissionDetails` *Object* - additional details you can add to the submission
  * `targetId` *String* - specify the ID of a message. E.g. the message that was saved upon approval.
  * `comment` *String* - allows you to add an optional comment to the submission.
* `cb` *Function* - a callback with signature `(err, submissionId)`

### `server.submissions.executeAndApprove(submissionId, approvedRecordDetails, submissionDetails, cb)`

Handles executing a proposal by either creating or updating the record, with the fields passed in to `approvedRecordDetails`. It then calls the `approve` method.

* `submissionId` *String* - the cypherlink for the target submission (the `msgId` of the root message for the submission)
* `approvedRecordDetails` *Object* - similar to *recordDetails* in the *propose* methods, but it only holds fields and values that have been approved. These will be saved to the record being created or updated.
* `submissionDetails` *Object* - additional details you can add to the submission
  * `comment` *String* - allows you to add an optional comment to the submission.
* `cb` *Function* - a callback with signature `(err, submissionId)`

---

### `server.submissions.reject(submissionId, { comment }, cb)`

Updates a submission record by setting server.id to rejectedBy and adding the comment

* `submissionId` *String* - the cypherlink for the target submission (the `msgId` of the root message for the submission)
* `comment` *{ String }* - allows you to add an optional comment to the submission.
* `cb` *Function* - a callback with signature `(err, submissionId)`

---

### `server.submissions.get(submissionId,  cb)`

Retrieves the submission record.

* `submissionId` *String* - the cypherlink for the target submission (the `msgId` of the root message for the submission)
* `cb` *Function* - a callback with signature `(err, data)`

---
### `server.submissions.tombstone(submissionId, { reason, allowPublic, undo }, cb)`

Tombstones the submission record.

* `submissionId` *String* - the cypherlink for the target submission (the `msgId` of the root message for the submission)
* `cb` *Function* - a callback with signature `(err, root)`


---

## Linking Submissions

See [Linking Submissions](./linked-submissions.md)
