const Strategy = require('@tangle/strategy')
const isObject = require('./is-object')

const PLACEHOLDER_MSG_ID = '%AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=.sha256'

function sortInput (spec, input) {
  return Object.keys(input)
    .reduce(
      (acc, key) => {
        if (key === 'recps') acc.recps = input[key]
        else if (key === 'authors') acc.authors = input[key]
        else if (key === 'allowPublic') acc.allowPublic = input[key]
        else if (key in spec.props) acc.props[key] = input[key]
        else if (key in spec.staticProps) acc.staticProps[key] = input[key]
        else if (key in spec.nextStepData) acc.nextStepData[key] = input[key]
        else acc.unknown[key] = input[key]

        return acc
      },
      {
        props: {},
        staticProps: {},
        nextStepData: {},
        authors: undefined, // TODO
        recps: undefined,
        allowPublic: false,
        unknown: {}
      }
    )
}

module.exports = function validateRecordDetails (recordDetails, crut, cb) {
  const strategy = new Strategy(crut.spec.props)

  if (!isObject(recordDetails)) return cb(Error('recordDetails must be an object'))

  const {
    props,
    staticProps,
    nextStepData,
    recps,
    // authors, TODO
    unknown
  } = sortInput(crut.spec, recordDetails)

  if (Object.keys(unknown).length) {
    return cb(Error(`recordDetails has unallowed inputs: ${Object.keys(unknown)}`))
  }

  let propsT

  try {
    propsT = strategy.mapFromInput(
      props,
      [strategy.identity()]
    )
  } catch (e) { return cb(Error('failed mapping props from input', { cause: e })) }

  const dummyContent = {
    type: crut.spec.type,
    ...nextStepData,
    ...staticProps,
    ...propsT,

    recps,
    tangles: {
      [crut.spec.tangle]: { root: null, previous: null }
    }
  }

  // HACK: here we are adding a special case to links
  // so that we can ignore when a child or parent id is missing
  // we need to do this because the link doesnt yet have one of the ids
  // and it will be plugged in later on when a submission is executed
  // this is related to linked submissions where a submission depends on the result
  // of another one. E.g. we have two submissions, ones to create a profile and another to link
  // that profile to another profile
  if (crut.spec.type === 'link/profile-profile/child' || crut.spec.type === 'link/profile-profile/partner') {
    if (!dummyContent.parent && !dummyContent.child) {
      return cb(Error(`recordDetails are invalid for the type: ${crut.spec.type}. Needs a parent or child id.`))
    }

    if (!dummyContent.parent) dummyContent.parent = PLACEHOLDER_MSG_ID
    if (!dummyContent.child) dummyContent.child = PLACEHOLDER_MSG_ID
  }

  if (!crut.isRoot(dummyContent)) {
    const errors = crut.isRoot.errors
    console.error(`Errors: ${JSON.stringify(errors, null, 2)}`)
    return cb(Error(`recordDetails are invalid for the type: ${crut.spec.type}`, { cause: errors }))
  }

  cb()
}
