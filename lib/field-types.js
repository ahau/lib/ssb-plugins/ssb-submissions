const Overwrite = require('@tangle/overwrite')
const SimpleSet = require('@tangle/simple-set')
const OverWriteFields = require('@tangle/overwrite-fields')
const { feedIdRegex } = require('ssb-ref')
const overwrite = (schema) => Overwrite({ valueSchema: schema })
const feedIdPattern = feedIdRegex.toString().slice(1, -1)

module.exports = {
  string: overwrite({ type: 'string' }),
  image: overwrite({ $ref: '#/definitions/image' }),
  integer: overwrite({ $ref: '#/definitions/integer' }),
  feedIdSet: SimpleSet(feedIdPattern),
  feedIdComments: OverWriteFields({ keyPattern: feedIdPattern, valueSchema: { type: 'string' } }),
  messageIdProp: overwrite({ $ref: '#/definitions/messageId' })
}
