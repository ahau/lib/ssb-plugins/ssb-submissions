const isObject = require('./is-object')

module.exports = function validateSubmissionDetails (submissionDetails, done) {
  if (!isObject(submissionDetails)) return done('submissionDetails must be an object')

  const { comment, unknown, groupId, targetId, source, moreInfo /* recps */ } = Object.keys(submissionDetails)
    .reduce(
      (acc, key) => {
        if (['recps', 'groupId', 'comment', 'targetId', 'source', 'moreInfo'].includes(key)) acc[key] = submissionDetails[key]
        else acc.unknown[key] = submissionDetails[key]

        return acc
      },
      {
        groupId: null,
        targetId: null,
        comment: null,
        source: null,
        recps: null,
        moreInfo: null,
        unknown: {}
      }
    )

  if (Object.keys(unknown).length) {
    return done(`submissionDetails has unallowed inputs: ${Object.keys(unknown)}`)
  }

  // validate fields
  if (comment && typeof comment !== 'string') return done('submissionDetails.comment is not a string')
  if (source && typeof source !== 'string') return done('submissionDetails.source is not a string')

  // TODO: should we validate the groupId here or let crut do that
  if (groupId && typeof groupId !== 'string') return done('submissionDetails.groupId is not a string')
  if (targetId && typeof targetId !== 'string') return done('submissionDetails.targetId is not a string')

  if (moreInfo && typeof moreInfo !== 'object') return done('submissionDetails.moreInfo is not an object')

  // TODO: test recps fields, NOTE: a recp can be a feedId, groupId or poBoxId
  // if (recps && (!Array.isArray(recps) || !recps.every(recp => isFeedId(recp)))) return done('submissionDetails.recps is invalid')

  done(null, null)
}
