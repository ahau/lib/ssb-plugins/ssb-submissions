const { isMsg } = require('ssb-ref')

module.exports = function Get (ssb, submissionCrut) {
  return function get (submissionId, cb) {
    if (!isMsg(submissionId)) return cb(new Error('submissions.get requires a submissionId'))
    submissionCrut.read(submissionId, cb)
  }
}
