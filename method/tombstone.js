const { isMsgId } = require('ssb-ref')

module.exports = function Tombstone (submissionCrut) {
  return function tombstone (submissionId, { reason, allowPublic, undo }, cb) {
    const done = (err, data) => err
      ? cb(new Error(`submissions.tombstone ${err}`))
      : cb(null, data)

    if (!submissionId) return done('submissionId is missing')
    if (!isMsgId(submissionId)) return done('submissionId must be a valid msgId')

    submissionCrut.tombstone(submissionId, { reason, allowPublic, undo }, (err) => {
      if (err) {
        if (err.message.match(/not found in leveldb index/)) return done(`no submission found for submissionId: ${submissionId}`)
        return done(err)
      }

      // NOTE: if you create a submission and then tombstone it, the "update" message for the
      // tombstone doesnt contain the required fields (targetType, details, tangles)
      // so to avoid people from being able to do that, here we just return the original
      // messageId
      cb(null, submissionId)
    })
  }
}
