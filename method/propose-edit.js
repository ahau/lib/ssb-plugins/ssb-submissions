const { isMsgId } = require('ssb-ref')
const isObject = require('../lib/is-object')
const validateSubmissionDetails = require('../lib/validate-submission-details')

module.exports = function ProposeEdit (ssb, loadCrut, submissionCrut) {
  function validateRecord (recordId, recordDetails, done) {
    if (!recordId) return done('recordId is missing')
    if (!isMsgId(recordId)) return done('recordId must be a valid msgId')
    if (!isObject(recordDetails)) return done('recordDetails must be an object')

    loadCrut(recordId, (err, recordCrut) => {
      if (err) {
        if (err.message.match(/not found in leveldb index/)) return done(`no record found for recordId: ${recordId}`)
        return done(err)
      }

      if (!recordCrut) return done('No handler found for the recordId: ' + JSON.stringify(recordId))
      if (!recordCrut.isValidUpdateInput(recordDetails)) return done(`recordDetails are invalid for the type: ${recordCrut.spec.type}`)

      done(null, recordCrut.spec.type)
    })
  }

  return function proposeEdit (recordId, recordDetails, submissionDetails, cb) {
    const done = (err, data) => err
      ? cb(new Error(`submissions.proposeEdit ${err}`))
      : cb(null, data)

    validateRecord(recordId, recordDetails, (err, recordType) => {
      if (err) return done(err)

      validateSubmissionDetails(submissionDetails, (err) => {
        if (err) return done(err)

        const { comment, recps, groupId } = submissionDetails

        const content = {
          sourceId: recordId,
          // targetId
          targetType: recordType,

          details: recordDetails, // TODO: Recps for final record in here

          groupId,
          recps // Who is allowed to see this submission. The targetRecord may have different recps.
          // TODO: Submissions should only be seen by kaitiaki group
        }

        if (comment) content.comments = { [ssb.id]: comment }
        submissionCrut.create(content, cb)
      })
    })
  }
}
