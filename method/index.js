const CRUT = require('ssb-crut')
const msgContent = require('ssb-msg-content')
const Submissions = require('../spec/submissions')

module.exports = function Method (ssb) {
  // An array of pairs that handle getting a crut of the proposedRecord
  const handlers = [] // [function isType(content), crut]
  const submissionCrut = new CRUT(ssb, Submissions, { alwaysIncludeStates: true })

  function findRecordCrut (msg) {
    const handler = handlers.find(pair => pair[0](msgContent(msg)))
    if (handler) return handler[1]
  }

  function registerHandler (crut) {
    if (!crut) throw Error('crut should not be null')
    if (
      !crut.spec ||
      !crut.spec.type ||
      !crut.isRoot ||
      !crut.isUpdate
    ) throw Error('Invalid crut registered as handler: ' + crut)

    const canHandle = (content) => {
      if (content.type !== crut.spec.type) return false

      if (content.type && Object.keys(content).length === 1) {
        return true
      }

      const result = crut.isValidUpdateInput(content)
      if (result) return true

      // if it's a new proposal?
      return crut.isRoot({
        ...content,
        tangles: {
          [crut.spec.tangle]: {
            root: null,
            previous: null
          }
        }
      })
    }

    // NOTE this may not be perfect, but we needed it to distinguish between
    // e.g. PersonProfileGroup + PersonProfileAdmin...

    handlers.push([canHandle, crut])
  }

  function loadCrut (id, cb) {
    ssb.get({ id, private: true, meta: true }, (err, root) => {
      if (err) return cb(err)
      const crut = findRecordCrut(root.value.content)
      if (crut) cb(null, crut)
      else cb(new Error('No CRUT registered for type ' + root.value.content.type))
    })
  }

  return {
    registerHandler,
    findHandler: findRecordCrut,

    proposeNew: require('./propose-new')(ssb, findRecordCrut, submissionCrut),
    proposeEdit: require('./propose-edit')(ssb, loadCrut, submissionCrut),
    proposeTombstone: require('./propose-tombstone')(ssb, loadCrut, submissionCrut),

    approve: require('./approve')(ssb, submissionCrut),
    executeAndApprove: require('./execute-and-approve')(ssb, findRecordCrut, submissionCrut),
    reject: require('./reject')(ssb, submissionCrut),

    get (submissionId, cb) {
      return submissionCrut.read(submissionId, cb)
    },
    list (opts, cb) {
      if (typeof opts === 'function') return submissionCrut.list({}, opts)
      return submissionCrut.list(opts, cb)
    },
    link: require('./link')(ssb),
    tombstone: require('./tombstone')(submissionCrut)
  }
}
