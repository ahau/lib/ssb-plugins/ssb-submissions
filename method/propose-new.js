const isObject = require('../lib/is-object')
const validateRecordDetails = require('../lib/validate-record-details')
const validateSubmissionDetails = require('../lib/validate-submission-details')

module.exports = function ProposeNew (ssb, findRecordCrut, submissionCrut) {
  function validateRecord (recordTypeContent, recordDetails, cb) {
    if (!recordTypeContent) return cb(Error('recordTypeContent is missing'))
    if (!isObject(recordTypeContent)) return cb(Error('recordTypeContent must be an object'))

    const recordCrut = findRecordCrut(recordTypeContent)
    if (!recordCrut) return cb(Error('No handler found for the recordTypeContent: ' + JSON.stringify(recordTypeContent)))

    validateRecordDetails(recordDetails, recordCrut, cb)
  }

  return function proposeNew (recordTypeContent, recordDetails, submissionDetails, cb) {
    validateRecord(recordTypeContent, recordDetails, (err) => {
      if (err) return cb(Error('submissions.proposeNew failed validating record', { cause: err }))

      validateSubmissionDetails(submissionDetails, (err) => {
        if (err) return cb(Error('submissions.proposeNew failed validating submission details', { cause: err }))

        const { comment, recps, groupId, source, moreInfo } = submissionDetails

        const { type } = recordTypeContent

        const content = {
          // sourceId: '',
          // targetId: '',
          targetType: type,

          details: recordDetails, // TODO: Recps for final record in here

          source, // Where the record was created e.g. ahau or webForm
          moreInfo, // additional details included in the submission
          groupId, // Keep a copy of the groupId, required
          recps // Who is allowed to see this submission. The targetRecord may have different recps.
          // TODO: Submissions should only be seen by kaitiaki group
        }

        if (comment) content.comments = { [ssb.id]: comment }
        submissionCrut.create(content, (err, res) => {
          if (err) return cb(Error('submissions.proposeNew failed creating submission crut', { cause: err }))
          return cb(null, res)
        })
      })
    })
  }
}
