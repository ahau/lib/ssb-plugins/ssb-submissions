const { feedIdSet, feedIdComments, messageIdProp } = require('../lib/field-types')
const { cloakedMessageId, messageId } = require('ssb-schema-definitions')()

const Spec = {
  type: 'submissions', // Maybe include new/edit/tombstone
  tangle: 'submissions',
  staticProps: {
    // Static to avoid any changes after approveance/rejection
    sourceId: {
      type: ['string', 'null'],
      pattern: messageId.pattern
    },
    targetType: { type: 'string', required: true },
    groupId: {
      type: 'string',
      pattern: cloakedMessageId.pattern
      // required: true TODO: cant be required because of public profiles
    },
    details: { type: 'object', required: true }, // TODO: rename details?
    source: { enum: ['ahau', 'webForm'] },
    moreInfo: {
      type: 'object',
      patternProperties: {
        '^[0-9a-zA-Z]+$': {
          nullable: true,
          oneOf: [
            // NOTE: we can add more types in here as we need them
            // { type: 'string' }, // for text
            // { type: 'boolean' }, // for checkbox
            // { type: 'number' }, // for numbers including ints and floats
            // stringArray // for array and list

            // this part specifically for an array of joining question answers, to be used with web registrations
            {
              type: 'array',
              items: {
                type: 'object',
                required: ['label', 'value'],
                properties: {
                  label: { type: 'string' },
                  value: { type: 'string' }
                },
                additionalProperties: false
              }
            }
          ]
        }
      },
      additionalProperties: false
    }
  },
  props: {
    targetId: messageIdProp,
    approvedBy: feedIdSet,
    rejectedBy: feedIdSet,
    comments: feedIdComments
    // A pointer to the target record update/create message once it has been created
  },
  hooks: {
    // isRoot: [
    //   DateChecker('createdAt')
    // ],
    // isUpdate: [
    //
    // ]
  }

  // TODO: A person shouldn't be able to approve their own submissions
  // Using isValidNextStep
}

module.exports = Spec
